# Rational

I find its hard to change my Mac terminal for Java Environment. 
I Had used some of package from `brew` but still not comfortable to switch between Java Environment.


# Limitations

* Only working on Mac OS (darwin) through exucuting `/usr/libexec/java_home` to get list of JDK installed.
* Do configuration to the shell's `rc` file by appending source file. If you want it to take effect immediately, refresh your shell or execute `source ~/.<your-shell-rc>`
* Currently only working for `bash` and `zsh` shell (mine is `zsh`).
* GraalVM Native Image binary is not consistent when listing installed JDK.
	
# How To Used

1. Make sure you have installed JDK and `clojure` through `brew` or any package manager.
2. Clone this repo and enter directory you've cloned,
3. Execute `clojure -X:uberjar`. If compiling is success, You'll get `uberjar` created inside `dist` directory.
4. Copy `javaenv.jar` file you've found inside `dist` directory to your local path, eg: `~/.local/bin`.
5. Append this line to your `rc` file:
``` bash
alias javaenv='~/.local/bin/javaenv'
```
6. Refresh your shell to take effect or execute `source ~/.<your-shell-rc>`.
7. Test the alias by executing `javaenv list`
