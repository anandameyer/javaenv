(ns anandameyer.javaenv.info
  (:require ;; [clojure.java.shell :as shell]
            [clojure.string :as string]
            [anandameyer.javaenv.file :as file]
            [clojure.pprint :as pprint]))

(def ident-list-headers [:index :name :version :arch :path :current?])

;; (defn- get-current-env
;;   []
;;   (->> (shell/sh "env")
;;        :out
;;        string/split-lines
;;        (map (fn [line]
;;               (let [[k v] (string/split line #"=")]
;;                 [(keyword k) v])))
;;        (into {})))

(defn os-type
  [{{:keys [name]} :os}]
  (let [lower-name (string/lower-case name)]
    (cond
      (string/index-of lower-name "win") :windows
      (string/starts-with? lower-name "mac") :darwin
      (string/starts-with? lower-name "nix") :linux
      (string/starts-with? lower-name "nux") :linux
      :else :unknown)))

(defn current-env-info
  []
  (let [props (System/getProperties)
        os-name (get props "os.name")
        os-version (get props "os.version")
        os-arch (get props "os.arch")
        jvm-name (get props "java.vm.name")
        jvm-version (get props "java.vm.version")
        jvm-vendor (get props "java.vm.vendor")
        user-home (get props "user.home")
        user-pwd (get props "user.dir")
        user-name (get props "user.name")
        java-version (get props "java.version")
        ;; {:keys [PWD SHELL USER HOME]} (get-current-env)
        ]
    {:os {:name os-name
          :version os-version
          :arch os-arch}
     :jvm {:name jvm-name
           :version jvm-version
           :vendor jvm-vendor
           :java java-version}
     :user {:name user-name
            :home user-home
            ;; :shell SHELL
            :pwd user-pwd}}))


(defn zshenv
  []
  (let [{{:keys [home]} :user} (current-env-info)]
    (file/any-file? (str home "/.zshenv"))))

(defn zshrc
  []
  (let [{{:keys [home]} :user} (current-env-info)]
    (file/any-file? (str home "/.zshrc"))))

(defn bashrc
  []
  (let [{{:keys [home]} :user} (current-env-info)]
    (file/any-file? (str home "/.bashrc"))))

(defn bashenv
  []
  (let [{{:keys [home]} :user} (current-env-info)]
    (file/any-file? (str home "/.bashenv"))))

(defn shell-type
  ([]
   (shell-type (current-env-info)))
  ([{{:keys[home]} :user}]
   (cond
     (file/any-file? (str home "/.zshrc")) :zsh
     (file/any-file? (str home "/.bashrc")) :bash
     :else :unsuported)))

(defn table-list-info!
  ([]
   (table-list-info! (current-env-info) nil))
  ([_]
   (table-list-info! (current-env-info) nil))
  ([info _]
   (let [{:keys[os jvm user]} info]
     (print "## OS Info:")
     (pprint/print-table [:name :version :arch] [os])
     (println)
     (print "## JVM Info:")
     (pprint/print-table [:name :version :vendor] [jvm])
     (println)
     (print "## User Info:")
     (pprint/print-table [:name :home :shell :pwd] [user]))))


(defn table-list-jvm!
  ([{:keys [list]}]
   (table-list-jvm! list nil))
  ([{:keys [list]} _]
   (pprint/print-table ident-list-headers list)))
