(ns anandameyer.javaenv.list-dispatcher
  (:require [anandameyer.javaenv.info :as info]
            [anandameyer.javaenv.darwin :as darwin]))

(defmulti jvms-list (fn[info] (info/os-type info)))

(defmethod jvms-list :darwin
  [_]
  (darwin/jvm-list))

(defmethod jvms-list :default
  [_]
  (throw (Exception. "OS not supported yet")))
