(ns anandameyer.javaenv.darwin
  (:require ;; [clojure.java.shell :as shell]
   [clojure.string :as string]
   [clojure.java.io :as io])
  (:import (java.io StringWriter)))

(def darwin-jvm-list-comm ["/usr/libexec/java_home" "-V"])
(def java-version-comm ["java" "-version"])


(defn parse-version
  [version]
  (let [[v a] (string/split version #",\s+")]
    {:version v
     :arch (string/replace a #":" "")}))

(defn parse-list-line
  [line]
  (->> (string/split line #"\t|\"")
       (map string/trim)
       (remove empty?)))

(defn exec-command
  [comms]
  (let [builder (ProcessBuilder. (into-array String comms))
        process (.start builder)
        out (.getInputStream process)
        err (.getErrorStream process)
        error-writer (StringWriter.)
        out-writer (StringWriter.)]
    (with-open [writer error-writer]
      (io/copy err writer :encoding "UTF8"))
    (with-open [writer out-writer]
      (io/copy out writer :encoding "UTF8"))
    (.destroy process)
    {:out (str out-writer)
     :err (str error-writer)}))

(defn- java-version
  []
  (let [{:keys[err]} (exec-command java-version-comm)
        version (->> (string/split-lines err)
                     first
                     (re-find #"\d+\.\d+\.\d+_?\d{0,}"))]
    {:version version}))

(defn jvm-list
  []
  (let [{:keys[err]} (exec-command darwin-jvm-list-comm)
        {:keys[version]} (java-version)]
    (->> err
         (string/split-lines)
         rest
         (reduce (fn[{:keys [list] :as init} out]
                   (let [[ver name path] (parse-list-line out)
                         parsed-version (parse-version ver)
                         current? (or (= version (:version parsed-version)) nil)
                         index (count list)
                         result (assoc parsed-version :index index :name name :path path :current? current?)]
                     (cond-> init
                       true     (update :list conj result)
                       current? (update :default (constantly result)))))
                 {:list[] :default {}}))))
