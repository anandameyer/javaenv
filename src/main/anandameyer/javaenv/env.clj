(ns anandameyer.javaenv.env
  (:require [anandameyer.javaenv.file :as file]
            [anandameyer.javaenv.info :as info]
            [clojure.string :as string]))

(defn comment-x
  [^String line]
  (if (and (seq line)
           (string? line))
    (if (and (string/includes? line "JAVA_HOME")
             (not (string/starts-with? line "#")))
      (str "# " line " ##=> This Auto Commented")
      line)
    line))

(defn java-env->comment!
  [^String path]
  (let [backup-file (file/backup-file path)]
    (file/copy-file backup-file path comment-x)))

(defn attach-env-file!
  [^String filepath {:keys [path]}]
  (let [sep "##=============Auto Created=============##\n"
        l1 (format "export JAVA_HOME=%s\n" path)
        l2 (format "export PATH=%s/bin:$PATH\n" path)
        comment " ## this line is auto added"
        appended (str "\n" (format "source %s" file/java-env-file) comment "\n")]
    (spit file/java-env-file (apply str [sep l1 l2 sep]))
    (spit filepath appended :append true)))


(defmulti set-env! (fn[current-info _ _] (info/shell-type current-info)))

(defmethod set-env! :zsh
  [_ jvms {:keys [index]}]
  (if (> index -1)
    (let [[rc :as files] [(info/zshrc) (info/zshenv)]
          {:keys [path] :as jvm} (-> jvms :list (get index))]
      (when (seq path)
        (doseq [f files]
          (when (seq f)
            (java-env->comment! f)))
        (attach-env-file! rc jvm)))
    (println "NOOP: no option given")))

(defmethod set-env! :bash
  [_ jvms {:keys [index]}]
  (if (> index -1)
    (let [[rc :as files] [(info/bashrc) (info/bashenv)]
          {:keys [path] :as jvm} (-> jvms :list (get index))]
      (when (seq path)
        (doseq [f files]
          (when (seq f)
            (java-env->comment! f)))
        (attach-env-file! rc jvm)))
    (println "NOOP: no option given")))

;; (defn auto-set!
;;   ([]
;;    (doseq [f [(info/zshenv) (info/zshrc)]]
;;      (java-env->comment! f))
;;    (attach-env-file!)))
