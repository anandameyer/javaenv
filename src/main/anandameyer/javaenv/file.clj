(ns anandameyer.javaenv.file
  (:require [clojure.java.io :as io]
            [clojure.string :as string]))

(def user-home-dir (System/getProperty "user.home"))
(def java-env-file (str user-home-dir "/.javaenv"))

(set! *warn-on-reflection* true)

(defn any-file?
  [path]
  (let [^java.io.File file (io/file path)]
    (when (.exists file)
      (.getPath file))))

(defn copy-file
  ([src dst]
   (copy-file src dst identity))

  ([src dst line-x]
   (when (any-file? src)
     (with-open [reader (io/reader src)
                 writer (io/writer dst)]
       (let [ls (->> reader
                     line-seq
                     (interleave (cycle ["\n"]))
                     rest)]
         (doseq [line ls]
           (let [ln (line-x line)
                 len (count ln)]
             (.write ^java.io.BufferedWriter writer ^String ln 0 len))
           )))
     dst)))

(defn move-file
  ([src dst]
   (move-file src dst identity))
  ([src dst line-x]
   (when (any-file? src)
     (copy-file src dst line-x)
     (.delete (^java.io.File io/file src))
     dst)))


(defn backup-file
  ([src]
   (let [src-dir (->> (string/split src #"/") butlast (string/join "/"))]
     (backup-file src src-dir)))
  ([src dst-dir]
   (backup-file src dst-dir ".backup"))
  ([src dst-dir extensions]
   (let [filename (-> (string/split src #"/") last)
         dst (str dst-dir "/" filename extensions)]
     (move-file src dst (fn[line]
                          (if (string/includes? line java-env-file)
                            "" line))))))
