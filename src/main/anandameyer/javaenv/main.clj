(ns anandameyer.javaenv.main
  (:require [cli-matic.core :refer [run-cmd]]
            [anandameyer.javaenv.info :as info]
            [anandameyer.javaenv.env :refer [set-env!]]
            [anandameyer.javaenv.list-dispatcher :refer [jvms-list]])
  (:gen-class))

;; (def current-info (info/current-shell-info))
;; (def jvms (jvms-list current-info))

(defn prepare-data
  []
  (let [cinfo (info/current-env-info)]
    [cinfo (jvms-list cinfo)]))

(def cli-config
  {:app         {:command     "javaenv"
                 :description "Another (yet) Java Environment Configurator"
                 :version     "0.0.1"}
   :commands    [{:command     "info" :short "i"
                  :description ["Print current environment information."]
                  :runs        (partial info/table-list-info! (info/current-env-info))}
                 {:command     "list"  :short "l"
                  :description ["List all jvm installed."]
                  :runs        (partial info/table-list-jvm! (second (prepare-data)))}
                 {:command     "set"  :short "s"
                  :description ["Set JVM to selected index."]
                  :opts        [{:option "index" :short "i" :as "Index shown by table" :type :int :default -1}]
                  :runs        (apply partial set-env! (prepare-data))}]})


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (run-cmd args cli-config))
