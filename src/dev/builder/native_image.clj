(ns builder.native-image
  (:require [clojure.java.shell :as shell]
            [clojure.java.io :as io]
            [clojure.string :as string]))


(def graal-home (System/getenv "GRAALVM_HOME"))
(def graal-bin-dir (str graal-home "/bin"))
(def path (System/getenv "PATH"))
(def path-with-graal (str graal-bin-dir ":" path))
(def graal-native-image (str graal-bin-dir "/native-image"))
(def current-java-home (System/getenv "JAVA_HOME"))

(def default-native-image-opts
  ["-H:Name=%s"
   "-H:+ReportExceptionStackTraces"
   "-H:CCompilerOption=-pipe"
   "-H:+PrintClassInitialization"
   ;; "-J-Dclojure.spec.skip.macros=true"
   ;; "-J-Dclojure.compiler.direct-linking=true"
   "-J-Xmx3G"
   "--report-unsupported-elements-at-runtime"
   "--native-image-info"
   "--allow-incomplete-classpath"
   "--verbose"
   "--no-fallback"])

(def error-msg-native-image-not-found
  (-> ["Error: native-iamge binary not found, please make sure"
       "1. You've installed GraalVM `https://www.graalvm.org/downloads/`."
       "2. You've set shell env GRAALVM_HOME to pointing to GraalVM installation directory."
       "3. You've install native image executor by executing `gu install native-image`"]
      (string/join "\n")))

(defn create-graal-env
  []
  {:PATH path-with-graal
   :JAVA_HOME graal-home
   :GRAAL_HOME graal-home})

(defn compile!
  [{:keys[jar-path binary-name graalvm-args] :or {binary-name "app"}}]
  (let [gni-exec (str graal-bin-dir "/native-image")
        build-args (-> default-native-image-opts
                       (update-in [0] #(format % binary-name))
                       (concat graalvm-args)) 
        com-seq (apply conj [gni-exec "-cp" jar-path "-jar" jar-path] build-args)]
    (if (.exists (io/file gni-exec))
      (do
        (println (format "Create Native Image with: \n %s" (string/join " " com-seq)))
        (shell/with-sh-env (create-graal-env)
          (let [{:keys[ out err]} (apply shell/sh com-seq)]
            (if err
              (println err)
              (println out)))))
      
      (println error-msg-native-image-not-found)))
  (shutdown-agents))
